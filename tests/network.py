import json
import unittest
from datetime import datetime
from http import HTTPStatus
import random

import requests

POLICE_API_URL = "http://127.0.0.1:8000/police/people/"
COLOMBIA_API_URL = "http://127.0.0.1:8000/colombia/people/"


class APITest(unittest.TestCase):

    @staticmethod
    def build_person(colombian=False):
        id_types = ['cc', 'ce', 'passport']
        genders = ['male', 'female']
        names = ['John', "Freddy", "Mathias", "Lenin", "Mauricio", "Camilo", "Monica", "Alexa"]
        last_names = ["Ramirez", "Toro", "Alcantara", "Guzman", "Rodriguez", "Giraldo", "Lopez"]
        addresses = ["Fake ST 123", "Evergreen st 742", "Baker St 221B"]
        domains = ["com", "net", "co", "io"]
        email_domains = ['email', 'gmail', 'hotmail', 'yahoo']

        data = {
            'name': random.choice(names),
            'last_name': random.choice(last_names),
            'id_type': random.choice([id_t for id_t in id_types]),
            'id_number': str(random.randint(1e5, 1e6)),
        }
        if colombian:
            data = {
                **data,
                'expedition_date': datetime.strftime(datetime.utcnow(), "%Y-%m-%d"),
                'age': random.randint(18, 65),
                'gender': random.choice([g for g in genders]),
                'address': random.choice(addresses),
                'phone_number': "".join([str(random.randint(1, 9)) for _ in range(10)]),
                'email': "{}{}@{}.{}".format(
                    data['name'][0], data['last_name'], random.choice(email_domains), random.choice(domains)
                ).lower()
            }

        return data

    @staticmethod
    def create_person_in_colombia_db():
        payload = APITest.build_person(colombian=True)
        return requests.post(COLOMBIA_API_URL, data=payload)

    @staticmethod
    def create_person_in_police_db():
        payload = APITest.build_person()
        return requests.post(POLICE_API_URL, data=payload)

    def test_create_colombian(self):
        response = APITest.create_person_in_colombia_db()
        self.assertEqual(response.status_code, HTTPStatus.CREATED)

    def test_retrieve_colombian(self):
        response = APITest.create_person_in_colombia_db()
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        data = json.loads(response.text)
        response = requests.get("{}{}".format(COLOMBIA_API_URL, data['url'][:-1].split('/')[-1]))
        self.assertEqual(response.status_code, HTTPStatus.OK)

    def test_create_police(self):
        response = APITest.create_person_in_police_db()
        self.assertEqual(response.status_code, HTTPStatus.CREATED)

    def test_retrieve_police(self):
        response = APITest.create_person_in_police_db()
        self.assertEqual(response.status_code, HTTPStatus.CREATED)
        data = json.loads(response.text)
        response = requests.get("{}{}".format(POLICE_API_URL, data['url'][:-1].split('/')[-1]))
        self.assertEqual(response.status_code, HTTPStatus.OK)
