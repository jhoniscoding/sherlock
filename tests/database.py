import unittest
from random import randint

from pymongo.results import InsertOneResult

from internal.database.connection import Connection
from internal.settings import DB_NAME

UNIT_TESTING = 'unit_testing'


class DatabaseTest(unittest.TestCase):

    def setUp(self):
        self.connection = Connection(connect=True)

    def test_connect(self):
        self.connection.start()
        server_info = self.connection.db_client.server_info()
        self.assertIn('ok', server_info, "Database connection could not be established!")
        self.connection.stop()

    def test_disconnect(self):
        self.connection.start()
        self.connection.stop()
        self.assertIsNone(self.connection.db_client, "Database connection could not be closed!")

    def test_insert_document(self):
        doc = self.connection.insert_document(DB_NAME, UNIT_TESTING, {'foo': 'bar'})
        self.assertIsInstance(doc, InsertOneResult, "Could not insert document!")

    def test_get_document(self):
        doc_id = randint(1, 100)
        self.connection.insert_document(DB_NAME, UNIT_TESTING, {'id': doc_id, 'foo': 'bar'})
        doc_found = self.connection.get_document(DB_NAME, UNIT_TESTING, str(doc_id))
        self.assertIsInstance(doc_found, dict, "Could not retrieve document!")
