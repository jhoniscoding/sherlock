import unittest

from internal.core.worker import WorkerFactory, DirectorWorker, IdentityWorker, CriminalWorker, ProspectWorker
from internal.settings import DIRECTOR_WORKER, IDENTITY_WORKER, CRIMINAL_WORKER, PROSPECT_WORKER


class WorkerTest(unittest.TestCase):

    def validate(self, worker_type, worker_class):
        instance = WorkerFactory.create(worker_type)
        self.assertIsInstance(instance, worker_class)

    def test_worker_factory(self):
        self.validate(DIRECTOR_WORKER, DirectorWorker)
        self.validate(IDENTITY_WORKER, IdentityWorker)
        self.validate(CRIMINAL_WORKER, CriminalWorker)
        self.validate(PROSPECT_WORKER, ProspectWorker)

    def test_worker_type_not_implemented(self):
        with self.assertRaises(NotImplementedError):
            WorkerFactory.create('test_worker')
