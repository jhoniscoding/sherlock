class PoliceRouter:
    """
    A router to control all database operations on models in the 'police' app.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read police models go to 'police_db' database.
        """
        if model._meta.app_label == 'police':
            return 'police_db'

        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write police models go to 'police_db' database.
        """
        if model._meta.app_label == 'police':
            return 'police_db'

        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the police 'app' is involved.
        """
        if obj1._meta.app_label == 'police' or obj2._meta.app_label == 'police':
            return True

        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the police app only appears in the 'police_db' database.
        """
        if app_label == 'police':
            return db == 'police_db'

        return None
