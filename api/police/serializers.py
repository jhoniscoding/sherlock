from rest_framework import serializers

from police.models import Person, CriminalHistory


class PersonSerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.ReadOnlyField()
    criminal_history = serializers.HyperlinkedRelatedField(
        many=True, read_only=True, view_name="criminalhistory-detail"
    )

    class Meta:
        model = Person
        fields = ('url', 'name', 'last_name', 'id_type', 'id_number', 'created_at', 'criminal_history')


class CriminalHistorySerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.ReadOnlyField()

    class Meta:
        model = CriminalHistory
        fields = ('url', 'person', 'reason', 'jail', 'sentence', 'created_at')
