from django.db import models
from django.utils import timezone


class Person(models.Model):
    CC = 'cc'
    CE = 'ce'
    PASSPORT = 'passport'
    ID_OPTIONS = [
        (CC, 'Cedula de ciudadania'),
        (CE, 'Cedula de extranjeria'),
        (PASSPORT, 'Pasaporte')
    ]

    MALE = 'male'
    FEMALE = 'female'
    GENDER_OPTIONS = [
        (MALE, 'Masculino'),
        (FEMALE, 'Femenino')
    ]

    name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    id_type = models.CharField(max_length=10, choices=ID_OPTIONS, default=CC)
    id_number = models.CharField(max_length=15)
    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        app_label = "police"

    def __str__(self):
        return "{} {}".format(self.name, self.last_name)


class CriminalHistory(models.Model):
    person = models.ForeignKey(Person, related_name="criminal_history", on_delete=models.CASCADE)
    reason = models.CharField(max_length=255)
    jail = models.BooleanField(default=False)
    sentence = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        app_label = "police"
