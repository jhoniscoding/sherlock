from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from police.models import Person, CriminalHistory
from police.serializers import PersonSerializer, CriminalHistorySerializer


class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all().order_by('-created_at')
    serializer_class = PersonSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('name', 'last_name', 'id_number')


class CriminalHistoryViewSet(viewsets.ModelViewSet):
    queryset = CriminalHistory.objects.all().order_by('-created_at')
    serializer_class = CriminalHistorySerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('person', 'jail', 'sentence')
