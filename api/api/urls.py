"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from rest_framework import routers
from rest_framework.documentation import include_docs_urls

from police.views import PersonViewSet, CriminalHistoryViewSet
from colombia.views import PeopleViewSet
from prospect.views import ProspectViewSet, PhoneViewSet, ContactViewSet

prospects_router = routers.DefaultRouter()
prospects_router.register(r'prospects', ProspectViewSet)
prospects_router.register(r'phones', PhoneViewSet)
prospects_router.register(r'contacts', ContactViewSet)

police_router = routers.DefaultRouter()
police_router.register(r'people', PersonViewSet)
police_router.register(r'criminal', CriminalHistoryViewSet)

colombia_router = routers.DefaultRouter()
colombia_router.register(r'people', PeopleViewSet)

urlpatterns = [
    path('prospects/', include(prospects_router.urls)),
    path('police/', include(police_router.urls)),
    path('colombia/', include(colombia_router.urls)),
    path('docs/', include_docs_urls(title='Sherlock API', public=False)),
]
