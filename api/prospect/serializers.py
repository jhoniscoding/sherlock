from rest_framework import serializers

from prospect.models import Prospect, Phone, Contact


class ProspectSerializer(serializers.HyperlinkedModelSerializer):
    phones = serializers.HyperlinkedRelatedField(many=True, queryset=Phone.objects.all(), view_name="phone-detail")
    contact = serializers.HyperlinkedRelatedField(read_only=True, view_name="contact-detail")
    created_at = serializers.ReadOnlyField()

    class Meta:
        model = Prospect
        fields = (
            'url', 'name', 'last_name', 'id_type', 'id_number', 'expedition_date',
            'email', 'age', 'gender', 'address', 'phones', 'contact',
            'created_at'
        )


class PhoneSerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.ReadOnlyField()

    class Meta:
        model = Phone
        fields = ('url', 'phone_type', 'number', 'person', 'created_at')


class ContactSerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.ReadOnlyField()
    score = serializers.ReadOnlyField()
    prospect = serializers.HyperlinkedRelatedField(read_only=True, view_name="prospect-detail")

    class Meta:
        model = Contact
        fields = ('url', 'score', 'prospect', 'created_at')
