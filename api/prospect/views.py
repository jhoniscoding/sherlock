from django_filters.rest_framework import DjangoFilterBackend
from prospect.models import Prospect, Phone, Contact
from prospect.serializers import ProspectSerializer, PhoneSerializer, ContactSerializer
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response


class ProspectViewSet(viewsets.ModelViewSet):
    queryset = Prospect.objects.all().order_by('-created_at')
    serializer_class = ProspectSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('name', 'last_name', 'id_number')

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        response = {
            'result': "Validation process started! You will receive a notification when it is completed"
        }
        return Response(response, status=status.HTTP_201_CREATED, headers=headers)


class PhoneViewSet(viewsets.ModelViewSet):
    queryset = Phone.objects.all().order_by('-created_at')
    serializer_class = PhoneSerializer


class ContactViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = Contact.objects.all().order_by('-created_at')
    serializer_class = ContactSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('prospect', 'score')

