from django.core.validators import MinValueValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from paho.mqtt import publish


class Prospect(models.Model):
    CC = 'cc'
    CE = 'ce'
    PASSPORT = 'passport'
    ID_OPTIONS = [
        (CC, 'Cedula de ciudadania'),
        (CE, 'Cedula de extranjeria'),
        (PASSPORT, 'Pasaporte')
    ]

    MALE = 'male'
    FEMALE = 'female'
    GENDER_OPTIONS = [
        (MALE, 'Masculino'),
        (FEMALE, 'Femenino')
    ]

    name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    id_type = models.CharField(max_length=10, choices=ID_OPTIONS, default=CC, verbose_name="ID Type")
    id_number = models.CharField(max_length=15, unique=True)
    expedition_date = models.DateField()
    email = models.EmailField(unique=True)
    age = models.IntegerField(validators=[MinValueValidator(18)])
    gender = models.CharField(max_length=15, choices=GENDER_OPTIONS)
    address = models.CharField(max_length=255)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return "{} {}".format(self.name, self.last_name)


class Phone(models.Model):
    LANDLINE = 'landline'
    MOBILE = 'mobile'
    PHONE_CHOICES = [
        (LANDLINE, 'Landline'),
        (MOBILE, 'mobile')
    ]
    phone_type = models.CharField(choices=PHONE_CHOICES, max_length=15)
    number = models.CharField(max_length=15)
    person = models.ForeignKey(Prospect, related_name="phones", on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.number


class Contact(models.Model):
    prospect = models.OneToOneField(Prospect, on_delete=models.CASCADE, primary_key=True)
    score = models.FloatField()
    created_at = models.DateTimeField(default=timezone.now)


@receiver(post_save, sender=Prospect, dispatch_uid="sherlock_uid")
def init_validation_process(sender, **kwargs):
    if kwargs['created']:
        publish.single('new_job', str(kwargs['instance'].id), hostname='34.68.109.95')
