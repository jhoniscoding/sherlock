from django.db import models
from django.utils import timezone


class Colombian(models.Model):
    CC = 'cc'
    CE = 'ce'
    PASSPORT = 'passport'
    ID_OPTIONS = [
        (CC, 'Cedula de ciudadania'),
        (CE, 'Cedula de extranjeria'),
        (PASSPORT, 'Pasaporte')
    ]

    MALE = 'male'
    FEMALE = 'female'
    GENDER_OPTIONS = [
        (MALE, 'Masculino'),
        (FEMALE, 'Femenino')
    ]

    name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    id_type = models.CharField(max_length=10, choices=ID_OPTIONS, default=CC, verbose_name="ID Type")
    id_number = models.CharField(max_length=15, unique=True)
    expedition_date = models.DateField()
    email = models.EmailField()
    age = models.IntegerField()
    gender = models.CharField(max_length=15, choices=GENDER_OPTIONS)
    phone_number = models.CharField(max_length=15, verbose_name="Phone number")
    address = models.CharField(max_length=255)
    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        app_label = "colombia"

    def __str__(self):
        return "{} {}".format(self.name, self.last_name)
