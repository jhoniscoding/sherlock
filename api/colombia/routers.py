class ColombiaRouter:
    """
    A router to control all database operations on models in the 'colombia' app.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read colombia models go to 'colombia_db' database.
        """
        if model._meta.app_label == 'colombia':
            return 'colombia_db'

        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write colombia models go to 'colombia_db' database.
        """
        if model._meta.app_label == 'colombia':
            return 'colombia_db'

        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the colombia 'app' is involved.
        """
        if obj1._meta.app_label == 'colombia' or obj2._meta.app_label == 'colombia':
            return True

        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the colombia app only appears in the 'colombia_db' database.
        """
        if app_label == 'colombia':
            return db == 'colombia_db'

        return None
