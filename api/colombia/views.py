from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from colombia.models import Colombian
from colombia.serializers import ColombianSerializer


class PeopleViewSet(viewsets.ModelViewSet):
    queryset = Colombian.objects.all().order_by('-created_at')
    serializer_class = ColombianSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('name', 'last_name', 'id_number')
