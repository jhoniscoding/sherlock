from rest_framework import serializers

from colombia.models import Colombian


class ColombianSerializer(serializers.HyperlinkedModelSerializer):
    created_at = serializers.ReadOnlyField()

    class Meta:
        model = Colombian
        fields = (
            'url', 'name', 'last_name', 'id_type', 'id_number', 'expedition_date',
            'email', 'age', 'gender', 'phone_number', 'address', 'created_at'
        )
