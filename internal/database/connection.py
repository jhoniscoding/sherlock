from mongoengine import connect
from pymongo import MongoClient

from internal.logging.logger import Logger
from internal.settings import DB_NAME, DB_PORT, DB_HOST
from internal.utils.singleton import Singleton


class Connection(metaclass=Singleton):

    def __init__(self, connect=False):
        self.conn_data = {
            'db': DB_NAME,
            'host': 'mongodb://{host}:{port}/{db_name}'.format(
                db_name=DB_NAME, port=DB_PORT, host=DB_HOST
            ),
            'connect': connect
        }
        self.db_client = None

    def start(self):
        self.db_client = connect(**self.conn_data)
        Logger().add_info_entry("Database connected!")

    def stop(self):
        self.db_client.close()
        self.db_client = None
        Logger().add_info_entry("Database disconnected!")

    def insert_document(self, db_name, collection_name, data):
        client = MongoClient(self.conn_data['host'])
        collection = client[db_name][collection_name]
        return collection.insert_one(data)

    def get_document(self, db_name, collection_name, doc_id):
        client = MongoClient(self.conn_data['host'])
        collection = client[db_name][collection_name]
        return collection.find_one({'id': int(doc_id)})
