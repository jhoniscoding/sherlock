import logging

from internal.utils.singleton import Singleton


class Logger(metaclass=Singleton):

    INFO = 'info'
    ERROR = 'error'

    def __init__(self, name=None):
        logging.basicConfig(
            format='%(asctime)s %(levelname)s %(name)s: %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S',
            level=logging.NOTSET
        )
        self.logger = logging.getLogger(name) if name is not None else logging.getLogger()

    def add_info_entry(self, message):
        self.logger.info(message)

    def add_error_entry(self, message, exc_info=False):
        if exc_info:
            self.logger.error("Exception occurred - {}".format(message), exc_info=exc_info)
        else:
            self.logger.error(message)
