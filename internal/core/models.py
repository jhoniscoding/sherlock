from datetime import datetime

import mongoengine as me
import pytz

from internal.core.topics import TOPICS
from internal.settings import WORKER_TYPES


class ValidationEmbedded(me.EmbeddedDocument):
    worker = me.StringField(choices=WORKER_TYPES, required=True)
    topic = me.StringField(choices=TOPICS.TOPICS_OPTIONS, required=True)
    required = me.BooleanField(default=True, required=True)
    result = me.BooleanField(default=None)
    data = me.DictField(default=None)


class Job(me.Document):

    CREATED = 'created'
    INITIAL_VALIDATIONS = 'initial_validations'
    FINAL_VALIDATIONS = 'final_validations'
    COMPLETED = 'completed'
    ACCEPTED = 'accepted'
    REJECTED = 'rejected'
    STATUS_OPTIONS = [CREATED, INITIAL_VALIDATIONS, FINAL_VALIDATIONS, COMPLETED]
    RESULT_OPTIONS = [ACCEPTED, REJECTED]

    prospect_id = me.StringField(required=True)
    status = me.StringField(choices=STATUS_OPTIONS, default=CREATED)
    result = me.StringField(choices=RESULT_OPTIONS, default=None)
    initial_validations = me.EmbeddedDocumentListField(ValidationEmbedded, default=None)
    final_validations = me.EmbeddedDocumentListField(ValidationEmbedded, default=None)
    created_at = me.DateTimeField(default=pytz.utc.localize(datetime.utcnow()))

    meta = {
        'collection': 'prospect_jobs'
    }
