import json
from abc import abstractmethod, ABC

from internal.core.models import Job, ValidationEmbedded
from internal.core.topics import TOPICS
from internal.logging.logger import Logger
from internal.settings import IDENTITY_WORKER, CRIMINAL_WORKER, PROSPECT_WORKER


class BaseCallback(ABC):

    NEW_WORK = 'new_work'
    NEW_JOB = 'new_job'
    WORK_DONE = 'work_done'

    def __init__(self, worker):
        self.worker = worker

    @abstractmethod
    def execute(self, *args, **kwargs):
        raise NotImplementedError("Not implemented yet!")


class NewWorkCallback(BaseCallback):

    def execute(self, *args, **kwargs):
        client, job_id = args[0], args[-1].payload.decode('utf8')
        job = Job.objects.get(id=job_id)
        self.worker.validate(job)
        payload = json.dumps({'job_id': job_id, 'worker_type': self.worker.worker_type})
        client.publish(TOPICS.WORK_DONE, payload)


class NewJobCallback(BaseCallback):

    def execute(self, *args, **kwargs):
        client, prospect_id = args[0], args[-1].payload.decode('utf8')
        job_data = {
            'prospect_id': prospect_id,
            'initial_validations': [
                ValidationEmbedded(worker=IDENTITY_WORKER, topic=TOPICS.IDENTITY),
                ValidationEmbedded(worker=CRIMINAL_WORKER, topic=TOPICS.CRIMINAL_HISTORY)
            ],
            'final_validations': [
                ValidationEmbedded(worker=PROSPECT_WORKER, topic=TOPICS.PROSPECT_EVAL)
            ]
        }
        job = Job(**job_data).save()
        job_id = str(job.id)
        Logger(self.worker.client_id).add_info_entry(
            "Created new job {} for prospect {}".format(
                job_id, prospect_id
            )
        )
        for val in job.initial_validations:
            client.publish(val.topic, job_id)
        job.update(status=Job.INITIAL_VALIDATIONS)


class WorkDoneCallback(BaseCallback):

    def execute(self, *args, **kwargs):
        payload = args[-1].payload.decode('utf8')
        data = json.loads(payload)
        self.worker.validate(data)


class CallbackFactory:

    @staticmethod
    def get_callback_method(worker, callback_type):
        if callback_type == BaseCallback.NEW_WORK:
            callback_name = 'NewWork'
        elif callback_type == BaseCallback.NEW_JOB:
            callback_name = 'NewJob'
        elif callback_type == BaseCallback.WORK_DONE:
            callback_name = 'WorkDone'
        else:
            raise NotImplementedError("Callback not implemented yet!")

        target_class = "{}Callback".format(callback_name)

        return globals()[target_class](worker).execute
