from abc import ABC, abstractmethod
from datetime import datetime
from random import randint

import pytz
import requests
from paho.mqtt import client as mqtt

from internal.core.callbacks import CallbackFactory, BaseCallback
from internal.core.models import Job
from internal.core.topics import TOPICS
from internal.database.connection import Connection
from internal.logging.logger import Logger
from internal.settings import BROKER_HOSTNAME, BROKER_PORT, WORKER_TYPES, DIRECTOR_WORKER, IDENTITY_WORKER, \
    CRIMINAL_WORKER, PROSPECT_WORKER, MIN_SCORE


class Worker(ABC):

    def __init__(self, client_id, callbacks, worker_type, run_forever=False):
        self.client_id = "{}_{}".format(client_id, self._build_id())
        self.callbacks = callbacks
        self.worker_type = worker_type
        self.run_forever = run_forever
        self.logger = Logger(self.client_id)
        try:
            self.client = mqtt.Client(client_id=client_id)
            self.client.on_connect = self.on_connect
            self.client.connect(BROKER_HOSTNAME, port=BROKER_PORT)
        except Exception as ex:
            self.logger.add_error_entry(str(ex), exc_info=True)

    def on_connect(self, *args, **kwargs):
        self.logger.add_info_entry("Connected to broker successfully")
        self.callbacks['stop_execution'] = self.stop
        try:
            for key, val in self.callbacks.items():
                self.client.subscribe(key)
                self.client.message_callback_add(key, val)
                self.logger.add_info_entry("Subscribed to {}".format(key))
        except Exception as ex:
            self.logger.add_error_entry(str(ex), exc_info=True)

    def stop(self, *args, **kwargs):
        try:
            self.client.disconnect()
        except Exception as ex:
            self.logger.add_error_entry(str(ex), exc_info=True)
        self.logger.add_info_entry("Worker stopped!")

    def start(self):
        self.logger.add_info_entry("Worker started!")
        try:
            if self.run_forever:
                self.client.loop_forever()
            else:
                self.client.loop_start()
        except Exception as ex:
            self.logger.add_error_entry(str(ex), exc_info=True)

    def publish(self, topic, message):
        try:
            self.client.publish(topic, message)
        except Exception as ex:
            self.logger.add_error_entry(str(ex), exc_info=True)

    @abstractmethod
    def validate(self, *args, **kwargs):
        raise NotImplementedError("Not implemented yet!")

    @staticmethod
    def _build_id():
        return int(datetime.utcnow().timestamp())


class DirectorWorker(Worker):

    def __init__(self):
        kwargs = {
            'worker_type': DIRECTOR_WORKER,
            'client_id': 'dir',
            'callbacks': {
                TOPICS.NEW_JOB: CallbackFactory.get_callback_method(self, BaseCallback.NEW_JOB),
                TOPICS.WORK_DONE: CallbackFactory.get_callback_method(self, BaseCallback.WORK_DONE)
            },
            'run_forever': True
        }
        super(DirectorWorker, self).__init__(**kwargs)

    def validate(self, data):
        def contains_validation(val_list):
            return val_list.filter(worker=worker_type).first() is not None

        def validations_complete(val_list):
            return val_list.filter(required=True, result=None).count() == 0

        def add_new_contact():
            vld = job.final_validations.filter(worker=worker_type).first()
            conn = Connection()
            db_name = 'prospects_db'
            prospect = conn.get_document(db_name, 'prospect_prospect', job.prospect_id)
            contact_data = {
                'prospect_id': prospect['id'],
                'score': vld.data['score'],
                'created_at': pytz.utc.localize(datetime.utcnow())
            }
            conn.insert_document(db_name, 'prospect_contact', contact_data)

        job_id = data['job_id']
        worker_type = data['worker_type']
        job = Job.objects.get(id=job_id)

        if contains_validation(job.initial_validations) and job.status == Job.INITIAL_VALIDATIONS:
            if validations_complete(job.initial_validations):
                for val in job.final_validations:
                    self.client.publish(val.topic, job_id)
                job.update(status=Job.FINAL_VALIDATIONS)

        elif contains_validation(job.final_validations):
            if validations_complete(job.final_validations):
                validations = []
                validations.extend(job.initial_validations)
                validations.extend(job.final_validations)
                result = Job.ACCEPTED
                for val in validations:
                    if val.required and not val.result:
                        result = Job.REJECTED

                if result == Job.ACCEPTED:
                    add_new_contact()

                job.update(result=result, status=Job.COMPLETED)
                self.logger.add_info_entry("Prospect {} was {}!".format(job.prospect_id, result.lower()))

                self.logger.add_info_entry("All validations done!")

        self.logger.add_info_entry("{} Validation done!".format(worker_type))


class IdentityWorker(Worker):

    def __init__(self):
        kwargs = {
            'worker_type': IDENTITY_WORKER,
            'client_id': 'iden',
            'callbacks': {
                TOPICS.IDENTITY: CallbackFactory.get_callback_method(self, BaseCallback.NEW_WORK)
            }
        }
        super(IdentityWorker, self).__init__(**kwargs)

    def validate(self, job):
        def validate_identity():
            url = "http://127.0.0.1:8000/colombia/people/?id_number={}".format(id_number)
            response = requests.get(url).json()
            if response.get('count', 0) == 0:
                return False, "Person does not exist in Colombia DB"

            prosp = response.get('results')[0]
            reason = {
                'error_message': 'These prospect fields do not match with Identity DB',
                'fields': [
                    key for key in
                    ['name', 'last_name', 'email', 'age', 'gender', 'address']
                    if prosp[key] != prospect[key]
                ]
            }
            if prosp['expedition_date'] != datetime.strftime(prospect['expedition_date'], '%Y-%m-%d'):
                reason['fields'].append('expedition_date')

            return (False, reason) if reason['fields'] else (True, "Identity validated")

        self.logger.add_info_entry("Making identity validation!")
        prospect = Connection().get_document('prospects_db', 'prospect_prospect', job.prospect_id)
        id_number = prospect['id_number']
        iden_val = validate_identity()
        validation = job.initial_validations.get(worker=self.worker_type)
        validation.result = iden_val[0]
        validation.data = {'reason': iden_val[1]}
        job.save()


class CriminalWorker(Worker):

    def __init__(self):
        kwargs = {
            'worker_type': CRIMINAL_WORKER,
            'client_id': 'crim',
            'callbacks': {
                TOPICS.CRIMINAL_HISTORY: CallbackFactory.get_callback_method(self, BaseCallback.NEW_WORK)
            }
        }
        super(CriminalWorker, self).__init__(**kwargs)

    def validate(self, job):
        def validate_criminal_history():
            url = "http://127.0.0.1:8000/police/people/?id_number={}".format(id_number)
            response = requests.get(url).json()
            if response.get('count', 0) == 0:
                return False, "Person does not exist in Police DB"

            crim_rcrds = len(response.get('results')[0].get('criminal_history', []))
            res = (crim_rcrds == 0, "Person has {} criminal entries in Police DB".format(crim_rcrds))
            return res

        self.logger.add_info_entry("Making criminal validation!")
        prospect = Connection().get_document('prospects_db', 'prospect_prospect', job.prospect_id)
        id_number = prospect['id_number']
        crim_val = validate_criminal_history()
        validation = job.initial_validations.get(worker=self.worker_type)
        validation.result = crim_val[0]
        validation.data = {'reason': crim_val[1]}
        job.save()


class ProspectWorker(Worker):

    def __init__(self):
        kwargs = {
            'worker_type': PROSPECT_WORKER,
            'client_id': 'pros',
            'callbacks': {
                TOPICS.PROSPECT_EVAL: CallbackFactory.get_callback_method(self, BaseCallback.NEW_WORK)
            }
        }
        super(ProspectWorker, self).__init__(**kwargs)

    def validate(self, job):
        self.logger.add_info_entry("Making prospect validation!")
        score = randint(1, 100)
        validation = job.final_validations.get(worker=self.worker_type)
        validation.result = score >= MIN_SCORE
        validation.data = {'score': score}
        job.save()


class WorkerFactory:

    @staticmethod
    def create(worker_type):
        if worker_type not in WORKER_TYPES:
            raise NotImplementedError("Worker not implemented yet!")

        target_class = "{}Worker".format(worker_type.capitalize())
        return globals()[target_class]()
