class TOPICS:
    IDENTITY = 'identity'
    CRIMINAL_HISTORY = 'criminal_history'
    NEW_JOB = 'new_job'
    WORK_DONE = 'work_done'
    PROSPECT_EVAL = 'prospect_eval'
    TOPICS_OPTIONS = [IDENTITY, CRIMINAL_HISTORY, NEW_JOB, WORK_DONE, PROSPECT_EVAL]
