from internal.core.worker import WorkerFactory
from internal.database.connection import Connection
from internal.logging.logger import Logger
from internal.settings import IDENTITY_WORKER, CRIMINAL_WORKER, PROSPECT_WORKER, \
    DIRECTOR_WORKER


def connect_database():
    Connection().start()


def disconnect_database():
    Connection().stop()


def start_workers():
    Logger().add_info_entry("Starting workers!")
    WorkerFactory.create(IDENTITY_WORKER).start()
    WorkerFactory.create(CRIMINAL_WORKER).start()
    WorkerFactory.create(PROSPECT_WORKER).start()
    WorkerFactory.create(DIRECTOR_WORKER).start()


def start_system():
    connect_database()
    start_workers()


if __name__ == '__main__':
    Logger().add_info_entry("Internal system execution started!")
    start_system()
    disconnect_database()
    Logger().add_info_entry("Internal system execution finished!")
