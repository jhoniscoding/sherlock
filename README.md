# Sherlock v1.0

This project allows users to create prospects and make required validations
using multiple API systems.

To start the process go to Prospects API and insert a new prospect. The prospect info must match with the info stored in Identity system (Colombia) and with Police system. Be sure some records exist on those systems.

After the prospect is created a new Job is launched and processed by the internal system. When the validations are done, the result will be displayed in the terminal as a message.

# Requirements

Libraries required:
* `django = "2.2.2"` (https://www.djangoproject.com/)
* `djangorestframework = "3.9.4"` (https://www.django-rest-framework.org/)
* `djongo = "1.2.33"` (https://nesdis.github.io/djongo/)
* `mongodb = 4.0.3` (https://www.mongodb.com/)
* `markdown = "3.1.1"`
* `coreapi = "2.3.3"`
* `django-filter = "2.1.0"`
* `paho-mqtt = "1.4.0"` (https://www.eclipse.org/paho/)
* `mongoengine = "0.18.2"` (http://mongoengine.org/)
* `requests = "2.22.0"` (https://2.python-requests.org/en/master/)

Mongo daemon must be running in (Required for API systems and internal validation system):
* `HOST = 127.0.0.1`
* `PORT = 26099`

However these settings could be changed in:
* API's: `api/api/settings.py`
* Internal system: `internal/settings.py`

# Installation
Clone repo

`git clone https://github.com/jhoniscoding/sherlock.git`

Go into project folder

`cd sherlock`

Create virtual environment and install libraries ([Pipenv](https://docs.pipenv.org/en/latest/) is required)

`pipenv sync`

Open a terminal in the virtual environment

`pipenv shell`

Go into API's folder

`cd api`

Run the migration scripts for the API systems (Make sure the mongo server is running)

* Prospect API: `python manage.py migrate` 
* Identity API: `python manage.py migrate colombia --database colombia_db`
* Police API: `python manage.py migrate police --database police_db`

# Usage

## Start API's server

`python api/manage.py runserver 8000`

Open the _Web browsable API_ in your browser for each API:

* Prospect API: [http://127.0.0.1:8000/prospects/](http://127.0.0.1:8000/prospects/) 
* Identity API: [http://127.0.0.1:8000/colombia/](http://127.0.0.1:8000/colombia/) 
* Police API: [http://127.0.0.1:8000/police/](http://127.0.0.1:8000/police/) 

To start using the API. Also it is possible to make requests to API using another REST clients like 
[Postman](https://www.getpostman.com/), [cURL](https://curl.haxx.se/), [Insomnia](https://insomnia.rest/), etc.

### API Docs

Docs are exposed in [http://127.0.0.1:8000/docs/](http://127.0.0.1:8000/docs/).

## Start internal system

`python internal/main.py`

All the workers will start and will keep listening for jobs to validate new prospects

# Unit tests

You can run these unit from root folder tests to validate that API's and internal system are working properly:

`python -m unittest discover -s tests -p "*.py"`

Enjoy!